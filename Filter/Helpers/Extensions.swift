//
//  Extensions.swift
//  Filter
//
//  Created by Emiray on 30.08.2020.
//  Copyright © 2020 Emiray. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

//MARK: - DEFAULTS KEYS
extension DefaultsKeys {
    
    static let overlaymodelelementArray = DefaultsKey<[OverlayModelElement]>("overlaymodelelementArray", defaultValue: [])
    static let overlaymodelArray = DefaultsKey<OverlayModel>("overlaymodelArray", defaultValue: [])
}

// MARK: - UICOLOR
extension UIColor {
    convenience init(hex: Int) {
        self.init(hex: hex, alpha: 1)
    }
    
    convenience init(hex: Int, alpha: Double) {
        self.init(
            red: CGFloat((hex >> 16) & 0xff) / 255,
            green: CGFloat((hex >> 8) & 0xff) / 255,
            blue: CGFloat(hex & 0xff) / 255,
            alpha: CGFloat(alpha))
    }
}
