//
//  ImageSaver.swift
//  Filter
//
//  Created by Emiray on 30.08.2020.
//  Copyright © 2020 Emiray Elektronik. All rights reserved.
//

import Foundation
import UIKit

class ImageSaver: NSObject {
    
    func writeToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveError), nil)
    }

    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Save finished!")
        
        let alertController = UIAlertController(title: "BAŞARILI", message: "Foto Fotoğraflar'a Kayıt Edildi !", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Tamam", style: .default)
        alertController.addAction(okAction)
        UIApplication.shared.windows[0].rootViewController!.present(alertController, animated: true)
        
    }
}
