//
//  OverlayModel.swift
//  Filter
//
//  Created by Emiray on 29.08.2020.
//  Copyright © 2020 Emiray Elektronik. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

// MARK: - OverlayModelElement
class OverlayModelElement: Codable, DefaultsSerializable {
    let overlayID: Int?
    let overlayName: String?
    let overlayPreviewIconURL, overlayURL: String?

    enum CodingKeys: String, CodingKey {
        case overlayID = "overlayId"
        case overlayName
        case overlayPreviewIconURL = "overlayPreviewIconUrl"
        case overlayURL = "overlayUrl"
    }

    init(overlayID: Int?, overlayName: String?, overlayPreviewIconURL: String?, overlayURL: String?) {
        self.overlayID = overlayID
        self.overlayName = overlayName
        self.overlayPreviewIconURL = overlayPreviewIconURL
        self.overlayURL = overlayURL
    }
}

typealias OverlayModel = [OverlayModelElement]

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }

            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }

            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }

    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }

    @discardableResult
    func responseOverlayModel(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<OverlayModel>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
