//
//  MainView.swift
//  Filter
//
//  Created by Emiray on 29.08.2020.
//  Copyright © 2020 Emiray Elektronik. All rights reserved.
//

import UIKit

class MainView: UIView {

    @IBOutlet var containerView: UIView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var buttonGallery: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("MainView", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
}
