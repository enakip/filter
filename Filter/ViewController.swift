//
//  ViewController.swift
//  Filter
//
//  Created by Emiray on 29.08.2020.
//  Copyright © 2020 Emiray Elektronik. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyUserDefaults

class ViewController: UIViewController {

    @IBOutlet weak var mainView : MainView!
    @IBOutlet weak var collectionview : UICollectionView!
    @IBOutlet weak var barbuttonDone : UIBarButtonItem!
    @IBOutlet weak var barbuttonClear : UIBarButtonItem!
    
    var overlayArray : OverlayModel = []
    
    // MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.buttonGallery.addTarget(self, action: #selector(addImage), for: .touchUpInside)
        self.barbuttonDone.target = self
        self.barbuttonDone.action = #selector(saveImage)
        
        self.barbuttonClear.target = self
        self.barbuttonClear.action = #selector(clearImage)
        
        self.barbuttonDisable(disable: true)
        
        if !Connectivity.isConnectedToInternet() {
           
            if Defaults[.overlaymodelArray].count > 0 {
                self.overlayArray = Defaults[.overlaymodelArray]
                self.collectionview.reloadData()
            }
            
        } else {
            self.getOverlays()
        }
        
    }
    
    // MARK: - GET OVERLAY DATA
    func getOverlays() {
        let urll : URL = URL.init(string: "https://lyrebirdstudio.s3-us-west-2.amazonaws.com/candidates/overlay.json")!
        
        Alamofire.request(urll).responseOverlayModel { (response) in
            if let overModelElement = response.result.value {
                print("YES !")
                
                let none : OverlayModelElement = OverlayModelElement.init(overlayID: 0,
                                                                          overlayName: "None", overlayPreviewIconURL: "none", overlayURL: "none")
                
                self.overlayArray = overModelElement
                self.overlayArray.insert(none, at: 0)
                
                Defaults[.overlaymodelArray].removeAll()
                Defaults[.overlaymodelArray] = self.overlayArray
                
                self.collectionview.reloadData()
                
            } else {
                print("NO !")
            }
        }
    }
    
    func barbuttonDisable(disable:Bool) {
        self.barbuttonDone.isEnabled = !disable
        self.barbuttonClear.isEnabled = !disable
    }
    
    // MARK: - ACTIONs
    // MARK: SAVE IMAGE TO LIBRARY
    func createImage(imagevw:UIImageView) {
        UIGraphicsBeginImageContextWithOptions(imagevw.bounds.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        imagevw.layer.render(in: context!)
        let imgs : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let imageSaver = ImageSaver()
        imageSaver.writeToPhotoAlbum(image: imgs)
    }
    
    @objc func saveImage() {
       
        self.createImage(imagevw: self.mainView.imageview)
        
    }
    
    // MARK: CLEAR IMAGE
    @objc func clearImage() {
        self.mainView.imageview.isHidden = true
        self.mainView.buttonGallery.isHidden = false
        
        self.barbuttonDisable(disable: true)
    }
    
    // MARK:  ADD IMAGE
    @objc func addImage() {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    
    // MARK: ACTION PAN
    @objc func actionPan(recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.mainView.containerView)
        
        if let view = recognizer.view {
            view.center = CGPoint(x: view.center.x + translation.x, y: view.center.y + translation.y)
        }
        recognizer.setTranslation(CGPoint.zero, in: self.mainView.containerView)
    }
    
    // MARK: ACTION PINCH
    @objc func actionPinch(recognizer: UIPinchGestureRecognizer) {
            if recognizer.state == .began{
                print("START PICNH")
            }
            if recognizer.state == .changed{
                print("START change")
                
                let overlayImgVw : UIImageView = self.mainView.viewWithTag(99) as! UIImageView
                
                overlayImgVw.transform = CGAffineTransform.init(scaleX: recognizer.scale,
                                                                y: recognizer.scale)
            }
            if recognizer.state == .ended{
                print("end PICNH")
            }
        
    }

}

// MARK: - COLLECTION VIEW METHODS
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.overlayArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OverlayCollectionViewCell",
                                                       for: indexPath) as! OverlayCollectionViewCell
        
        cell.viewcntrlr = self
        
        let overlayModelElement : OverlayModelElement = self.overlayArray[indexPath.row]
        
        let overlayPreviewIconStr : String = overlayModelElement.overlayPreviewIconURL ?? ""
        let overlayPreviewIconURL : URL = URL.init(string: overlayPreviewIconStr)!
        cell.imageviewOverlay.kf.setImage(with: overlayPreviewIconURL)
        
        let overlayName : String = overlayModelElement.overlayName ?? ""
        cell.labelFilter.text = overlayName
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? OverlayCollectionViewCell {
            cell.cellIndex = indexPath.row
        }
        
        // REMOVE SELECTED OVERLAY
        for vws in self.mainView.imageview.subviews {
            if vws.isKind(of: UIImageView.self) {
                let imgvc : UIImageView = vws as! UIImageView
                if imgvc.tag == 99 {
                    imgvc.removeFromSuperview()
                }
            }
        }
        
        let overlayModelElement : OverlayModelElement = self.overlayArray[indexPath.row]
        
        let overlayURLStr : String = overlayModelElement.overlayURL ?? ""
        let overlayURL : URL = URL.init(string: overlayURLStr)!
        
        let rect : CGRect = CGRect.init(x: self.mainView.center.x,
                                        y: self.mainView.center.y,
                                        width: 100.0,
                                        height: 100.0)
        let selectedOverlay : UIImageView = UIImageView.init(frame: rect)
        selectedOverlay.isUserInteractionEnabled = true
        selectedOverlay.tag = 99
        selectedOverlay.kf.setImage(with: overlayURL)
        self.mainView.imageview.isUserInteractionEnabled = true
        self.mainView.imageview.addSubview(selectedOverlay)
        
        let pan = UIPanGestureRecognizer.init(target: self, action: #selector(actionPan(recognizer:)))
        pan.minimumNumberOfTouches = 1
        pan.maximumNumberOfTouches = 1
        selectedOverlay.addGestureRecognizer(pan)
        
        let pinch = UIPinchGestureRecognizer.init(target: self, action: #selector(actionPinch(recognizer:)))
        selectedOverlay.addGestureRecognizer(pinch)
        
        
    }
    
}

// MARK: - COLLECTION VIEW CELL
class OverlayCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var vieww: UIView!
    @IBOutlet weak var imageviewOverlay: UIImageView!
    @IBOutlet weak var labelFilter: UILabel!
    
    var viewcntrlr : ViewController!
    
    override func awakeFromNib() {
        imageviewOverlay.layer.masksToBounds = true
        imageviewOverlay.layer.borderWidth = 1.0
        
    }
    
    var cellIndex: Int? {
        didSet {
            guard let index = cellIndex else { return }
            if index != 0 {
                imageviewOverlay.layer.borderColor = UIColor.init(hex: 0x6387BF).cgColor
                labelFilter.textColor = UIColor.init(hex: 0x6387BF)
                
            }else{
                imageviewOverlay.layer.borderColor = UIColor.clear.cgColor
                labelFilter.textColor = UIColor.lightGray
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            
            imageviewOverlay.layer.borderColor = isSelected ? UIColor.init(hex: 0x6387BF).cgColor : UIColor.clear.cgColor
            labelFilter.textColor = isSelected ? .blue : .lightGray
        }
    }
    
}

// MARK: IMAGE PICKER
extension ViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }

        let imageName = UUID().uuidString
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)

        if let jpegData = image.jpegData(compressionQuality: 0.5) {
            try? jpegData.write(to: imagePath)
            
            self.mainView.buttonGallery.isHidden = true
            self.mainView.imageview.isHidden = false
            self.barbuttonDisable(disable: false)
            self.mainView.imageview.image = UIImage.init(data: jpegData)
            
        }

        dismiss(animated: true)
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
}
